
let count = [0];

async function getCategories() {
    const res = await fetch(`/categories`);
    const resJson = await res.json();

    if (res.status === 200) {
        return resJson.categories;
    } else {
        throw Error;
    }
}

function listCategories(categories) {
    const selectNode = document.querySelector('#CreateService_Category');
    selectNode.innerHTML = '';
    for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        const option = document.createElement("option");
        option.value = category.id;
        option.innerHTML = `${category.category}`;

        selectNode.appendChild(option);
    }
}

async function showCategories() {
    try {
        const categories = await getCategories();
        if (categories.length === 0) {
            alert('Please create category first!')
            window.location.href = '/categories.html';
        }
        listCategories(categories);
    } catch (error) {
        console.log(error);
        alert('Cannot get categories');
    }
}

document.getElementById('add_setting').addEventListener('click', function (event) {

    count.push(count[count.length - 1] + 1);
    numInHTML = count[count.length - 1];

    const zoneDiv = document.createElement('div');
    zoneDiv.className = `zone`
    zoneDiv.id = `zone-${numInHTML}`;
    zoneDiv.innerHTML = (/*html*/`
    <div class="m-3 p-3 border border-secondary rounded-lg addsetting shadow p-3 mb-5 bg-white rounded" id="addsetting-${numInHTML}">
    <div class="text-right" onclick="deleteSetting(${numInHTML})"><i class="fas fa-times delsetting awesome-hover" id="delsetting${numInHTML}"></i></div>
    <h1>Setting ${numInHTML}</h1>
    <div class="form-group">
        <label for="CreateService_DayOfWeek${numInHTML}">Working Day</label>
        <select class="form-control CreateService_DayOfWeek" id="CreateService_DayOfWeek${numInHTML}">
            <option value="1">Mon</option>
            <option value="2">Tue</option>
            <option value="3">Wed</option>
            <option value="4">Thur</option>
            <option value="5">Fri</option>
            <option value="6">Sat</option>
            <option value="7">Sun</option>
        </select>
    </div>
    <label for="CreateService_Duration${numInHTML}">Duration</label>
    <div class="form-group row d-flex align-items-center">
        <div class="input-group col-4">
            <input type="number" class="form-control CreateService_Duration" id="CreateService_Duration${numInHTML}"
                aria-describedby="TipHourUnit${numInHTML}" required>
            <div class="input-group-prepend">
                <span class="input-group-text" id="TipHourUnit${numInHTML}">hours</span>
            </div>
        </div>
        <div class="text-center col-1">From</div>
        <div id="timeStart${numInHTML}" class="col-3">
            <input type="time" class="form-control input-time flatpickr-input timeStart" name="start_time${numInHTML}"
                placeholder="Start Time" readonly="readonly">
        </div>
        <div class="text-center col-1">To</div>
        <div id="timeEnd${numInHTML}" class="col-3">
            <input type="time" class="form-control input-time flatpickr-input timeEnd" name="end_time${numInHTML}"
                placeholder="End Time" readonly="readonly">
        </div>
    </div>
    <div class="input-group mb-2">
        <div class="input-group-prepend">
            <span class="input-group-text" id="tipMoneyUnit${numInHTML}">$</span>
        </div>
        <input type="number" class="form-control CreateService_Price" id="CreateService_Price${numInHTML}"
            aria-describedby="tipMoneyUnit${numInHTML}" required>
    </div>

    <div class="form-check custom-control custom-switch text-right">
  <input type="checkbox" class=" form-check-input custom-control-input displaySwitch" id="displaySwitch${numInHTML}">
  <label class="form-check-label custom-control-label" for="displaySwitch${numInHTML}">Hide Setting</label>
</div>

</div>
 `)
    document.getElementById('SettingRegion').appendChild(zoneDiv);
    time(numInHTML);
});

function time(num) {
    const start_time = document.querySelector(`input[name="start_time${num}"]`);
    const end_time = document.querySelector(`input[name="end_time${num}"]`);

    let timeFromPicker = flatpickr(start_time, config = {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        onOpen: function () {
            start_time.classList.remove('is-invalid');
            let maxTime = end_time.value;
            if (maxTime !== "") {
                timeFromPicker.config.maxTime = maxTime;
            }
        }
    });

    let timeToPicker = flatpickr(end_time, config = {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        onOpen: function () {
            end_time.classList.remove('is-invalid');
            let minTime = start_time.value;
            if (minTime !== "") {
                timeToPicker.config.minTime = minTime;
            }
        }
    });
}

function deleteSetting(num) {
    let delStr = document.querySelector(`#zone-${num}`);
    document.getElementById('SettingRegion').removeChild(delStr);
    let findNum = count.indexOf(num);
    if (findNum > -1) {
        count.splice(findNum, 1);
    }
}

document.getElementById("serviceRegister").addEventListener("submit", async function (event) {
    event.preventDefault();
    try {
        let formData = new FormData();
        formData.append('title', document.querySelector("#CreateService_title").value);
        formData.append('description', document.querySelector("#CreateService_Description").value);
        formData.append('image', document.querySelector("#CreateService_Image").files[0]);
        formData.append('category', document.querySelector("#CreateService_Category").value);
        let settingArray = [];
        if (document.querySelectorAll(".zone") != null) {
            for (i = 0; i < document.querySelectorAll(".zone").length; i++) {
                let workingDay = document.querySelectorAll(".CreateService_DayOfWeek")[i].value;
                let duration = document.querySelectorAll(".CreateService_Duration")[i].value;
                let startTime = document.querySelectorAll(".timeStart")[i].value;
                let endTime = document.querySelectorAll(".timeEnd")[i].value;
                let price = document.querySelectorAll(".CreateService_Price")[i].value;
                let status = document.querySelector(`#displaySwitch${i + 1}`).checked;
                const settingObject = { workingDay, duration, startTime, endTime, price, status };
                settingArray.push(settingObject);
            }
            formData.append("settings", JSON.stringify(settingArray))
        }

        console.log(formData);

        const res = await fetch('/service/create', {
            method: "POST",
            body: formData
        })

        if (res.status === 200) {
            window.alert("success");
            window.location.href = '/list_services.html';
        } else {
            const json = await res.json();
            window.alert(json.message);
        }

    } catch (error) {
        console.log("error");
        throw Error
    }
})

showCategories();
bsCustomFileInput.init();
