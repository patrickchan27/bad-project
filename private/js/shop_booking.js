async function getAllBookings() {
    const res = await fetch('/bookings');
    const resJson = await res.json();

    if (res.status === 200) {
        return resJson;
    } else {
        alert(resJson.message);
    }
}

function shopBookings(bookings) {
    const bookingsNode = document.querySelector("#bookings");
    bookingsNode.innerHTML = '';

    for (const booking of bookings) {
        const templateNode = document.querySelector(".booking-template").cloneNode(true);
        templateNode.classList.remove('booking-template');

        templateNode.querySelector(".booking-customer-name").innerHTML = booking.user.full_name;
        templateNode.querySelector(".booking-name").innerHTML = booking.service.title;
        templateNode.querySelector(".booking-date").innerHTML = moment(booking.date).format('YYYY-MM-DD');

        const fromTime = moment(booking.start_time, 'H:mm:ss').format('H:mm');
        const toTime = moment(booking.end_time, 'H:mm:ss').format('H:mm');
        templateNode.querySelector(".booking-time").innerHTML = fromTime + ' - ' + toTime;
        templateNode.querySelector(".booking-price").innerHTML = booking.price;
        templateNode.querySelector(".booking-phone").innerHTML = booking.user.phone;
        templateNode.querySelector(".booking-remark").innerHTML = booking.remark;
        bookingsNode.appendChild(templateNode);
    }
}

async function main() {
    try {
        const data = await getAllBookings();

        if (data.bookings) {
            shopBookings(data.bookings);
        }
    } catch (error) {
        console.log(error);
        alert('Error')
    }

}

main();
