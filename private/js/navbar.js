function navbar() { 
    htmlStr = ( /*html*/`<a class="navbar-brand" href="#">Logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item" id="bookingNav">
                <a class="nav-link" href="/shop_booking.html">Booking</a>
            </li>
            <li class="nav-item dropdown" id="serviceNav">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Services
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="./list_services.html">List</a>
                    <a class="dropdown-item" href="./create_services.html">Create new Service</a>
                    <a class="dropdown-item" href="/categories.html">Categories</a>
                </div>
            </li>
            <li class="nav-item" id="SettingNav">
                <a class="nav-link" href="#">Setting</a>
            </li>
        </ul>
    </div>
    <button class="btn btn-primary" id="logout" onclick="logOut()" >Logout</button>`);
    document.querySelector(`.navbar`).innerHTML = htmlStr;
}

async function logOut(){
    // document.querySelector("#logout").addEventListener("click", function(){
        await fetch('/users/logout', {method: "POST"});
        window.location.href = "/userlogin.html";
    // })
}

async function SeparateAccount() {
    const res = await fetch('/users/redirect');
    const jsonObj = await res.json();


    if (jsonObj.haveShop == true) {
        navbar();
    } else {
        navbar();
        document.querySelector('#serviceNav').style.display = "none"
    }
}

SeparateAccount();



