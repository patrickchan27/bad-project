
async function getCategories() {
    const res = await fetch(`/categories`);
    const resJson = await res.json();

    if (res.status === 200) {
        return resJson.categories;
    } else {
        throw Error;
    }
}

function listCategories(categories) {
    const tableNode = document.querySelector('#list');
    tableNode.querySelector('tbody').innerHTML = '';
    for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        const tr = document.createElement("tr");
        tr.innerHTML = `<tr><th scope="col">${i + 1}</th><th scope="col">${category.category}</th></tr>`;

        tableNode.querySelector('tbody').appendChild(tr);
    }
}

document.querySelector('#categories-form').addEventListener('submit', async function (e) {
    e.preventDefault();
    const name = document.querySelector('#name').value;
    if (name) {
        try {
            const res = await fetch('/categories', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify({ name })
            });
            const json = await res.json();
        
            if (res.status === 200) {
                await showCategories();
            } else {
                alert(json.message);
            }
        } catch (error) {
            console.log(error);
            alert('Cannot add category');
        }
    }
})

async function showCategories() {
    try {
        const categories = await getCategories();
        listCategories(categories);
    } catch (error) {
        console.log(error);
        alert('Cannot get categories');
    }
}

showCategories();
