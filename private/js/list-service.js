
async function listAll() {
    const res = await fetch('service/list');
    const listAllJsonRes = await res.json();

    const listServices = listAllJsonRes.services;
    console.log(listServices); // Array of Objects

    const listSettings = listAllJsonRes.settings;
    console.log(listSettings) // Array of Objects

    for (let i = 0; i < listServices.length; i++) {
        const serviceDiv = document.createElement('tr');
        serviceDiv.className = `ServiceRow table-success`;
        serviceDiv.id = `serviceRow${i + 1}`;
        serviceDiv.innerHTML = (/*html*/`
                <th scope="row">${i + 1}</th>
                <td>${listServices[i].title}</td>
                <td>${listServices[i].category_id}</td>
                <td>${listServices[i].description}</td>
                <td><a class="btn btn-additionSetting" role="button" href="#"><i class="fas fa-edit awesome-hover"></i></a>
                    <a class="btn btn-additionSetting" data-toggle="collapse" href="#collapseExample${i + 1}" role="button"
                        aria-expanded="false" aria-controls="collapseExample${i + 1}"><i class="fas fa-info-circle awesome-hover"></i></a>
                        </td>         
                    `)
        const settingDiv = document.createElement('tr');

        settingDiv.innerHTML = (/*html*/`
            <td colspan="5">
                            <div class="collapse" id="collapseExample${i + 1}">
                                <table class="table table-hover text-center table-striped">
                                    <tr class="bg-warning">
                                        <th scope="col">Setting #</th>
                                        <th scope="col">Working Day</th>
                                        <th scope="col">Duration</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Price</th>
                                    </tr>
                                    <tbody class="settingDetail", id="service${i + 1}Settinglist">
                                    </tody>
                                </table>
                            </div>
                        </td>
                        `)

        document.getElementById('allServiceList').appendChild(serviceDiv);
        document.getElementById('allServiceList').appendChild(settingDiv);

        let settings = listSettings[i];

        for (let a = 0; a < settings.length; a++) {
            const settingRow = document.createElement('tr');
            settingRow.id = `SettingList${i + 1}-Setting${a + 1}`

            settingRow.innerHTML = (/*html*/`      
            <td>${a + 1}</td>
            <td>${settings[a].working_day}</td>
            <td>${settings[a].duration} hours</td>
            <td>${settings[a].start_time} to ${settings[a].end_time}</td>
            <td>$${settings[a].price}</td>
        `)
            document.getElementById(`service${i + 1}Settinglist`).appendChild(settingRow);
        
        if(settings[a].status == true){
            document.getElementById(`SettingList${i + 1}-Setting${a + 1}`).style.display = "none";
        } 
        }

    }
}

listAll();
