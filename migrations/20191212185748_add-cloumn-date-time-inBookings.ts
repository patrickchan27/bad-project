import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    try {
        await knex.schema.table('bookings', function (table) {
            table.date('date');
            table.time('start_time');
            table.time('end_time');
        })
    } catch (error) {
        throw Error
}
}




export async function down(knex: Knex): Promise<any> {
    try{
        await knex.schema.table('bookings', function (table) {
            table.dropColumn('date');
            table.dropColumn('start_time');
            table.dropColumn('end_time');
        })
    }catch(error){
        throw Error
    }
}
