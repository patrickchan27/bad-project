import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    const trx = await knex.transaction();
    try {
        await trx.schema.createTable('users', (table) => {
            table.increments();
            table.string("username").unique().notNullable();
            table.string("password", 60).notNullable();
            table.string("email").unique().notNullable();
        });

        await trx.schema.createTable('shops', (table) => {
            table.increments();
            table.string("name").notNullable();
            table.text("description");
            table.integer("phone", 8).unique().notNullable().unsigned();
            table.string("address").notNullable();
            table.string("image");
            table.text("notice");
        });

        await trx.schema.createTable('roles', (table) => {
            table.increments();
            table.string("name").notNullable();
        });

        await trx.schema.createTable('status', (table) => {
            table.increments();
            table.string("status").notNullable();
        });

        await trx.schema.createTable('categories', (table) => {
            table.increments();
            table.integer("shop_id").unsigned();
            table.foreign("shop_id").references("shops.id");
            table.string("category").notNullable();
        });

        await trx.schema.createTable('day_week_name', (table) => {
            table.increments();
            table.string("name").notNullable().unique();
        });

        await trx.schema.createTable('shop_opening_hours', (table) => {
            table.integer("shop_id").unsigned();
            table.foreign("shop_id").references("shops.id");
            table.integer("day_of_the_week_id").unsigned();
            table.foreign("day_of_the_week_id").references("day_week_name.id");
            table.time("start_time").notNullable();
            table.time("end_time").notNullable();
        });

        await trx.schema.createTable('profiles', (table) => {
            table.increments();
            table.integer("phone", 8).notNullable().unsigned();
            table.string("image");
            table.string("full_name").notNullable();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
        });

        await trx.schema.createTable('shops_staff', (table) => {
            table.increments();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");

            table.integer("shop_id").unsigned();
            table.foreign("shop_id").references("shops.id");

            table.integer("role_id").unsigned();
            table.foreign("role_id").references("roles.id");
        });

        await trx.schema.createTable('services', (table) => {
            table.increments();
            table.integer("shop_id").unsigned();
            table.foreign("shop_id").references("shops.id");

            table.string("title").notNullable();
            table.text("description");
            table.integer("category_id").unsigned();
            table.foreign("category_id").references("categories.id");
            table.string("image");
            table.boolean("display").notNullable();
        });

        await trx.schema.createTable('service_setting', (table) => {
            table.increments();
            table.integer("service_id").unsigned();
            table.foreign("service_id").references("services.id");
            table.decimal("price", 8, 1).notNullable();
            table.integer("working_day").unsigned();
            table.foreign("working_day").references("day_week_name.id");
            table.integer("duration").notNullable();
            table.time("start_time").notNullable();
            table.time("end_time").notNullable();
            table.string("status").notNullable();
        });

        await trx.schema.createTable('bookings', (table) => {
            table.increments();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.integer("shop_id").unsigned();
            table.foreign("shop_id").references("shops.id");
            table.integer("status_id").unsigned();
            table.foreign("status_id").references("status.id");
            table.text("remake");
            table.timestamps(false, true);
        });

        await trx.schema.createTable('items', (table) => {
            table.increments();
            table.integer("booking_id").unsigned();
            table.foreign("booking_id").references("bookings.id");
            table.integer("service_id").unsigned();
            table.foreign("service_id").references("services.id");
            table.integer("service_setting_id").unsigned();
            table.foreign("service_setting_id").references("service_setting.id");
            table.decimal("price", 8, 1).notNullable();
            table.timestamps(false, true);
        });

        await trx.commit();
    } catch (err) {
        await trx.rollback();
        throw err;
    }
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTableIfExists('items'); //12
    await knex.schema.dropTableIfExists('bookings'); //11
    await knex.schema.dropTableIfExists('service_setting'); //10
    await knex.schema.dropTableIfExists('services'); //9
    await knex.schema.dropTableIfExists('shops_staff'); //8
    await knex.schema.dropTableIfExists('profiles'); //7
    await knex.schema.dropTableIfExists('shop_opening_hours'); //6
    await knex.schema.dropTableIfExists('day_week_name');
    await knex.schema.dropTableIfExists('categories'); //5
    await knex.schema.dropTableIfExists('status'); //4
    await knex.schema.dropTableIfExists('roles'); //3
    await knex.schema.dropTableIfExists('shops'); //2
    await knex.schema.dropTableIfExists('users'); //1
}

