import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    try {
        await knex.schema.table('bookings', function (table) {
           table.renameColumn('remake', 'remark')
        })
    } catch (error) {
        throw Error
}
}


export async function down(knex: Knex): Promise<any> {
    try {
        await knex.schema.table('bookings', function (table) {
           table.renameColumn('remark', 'remake')
        })
    } catch (error) {
        throw Error
}
}

