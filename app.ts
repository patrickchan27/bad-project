import * as path from 'path';

import * as express from "express";
import * as bodyParser from "body-parser";
import * as expressSession from 'express-session';
import * as multer from "multer";

import * as passport from 'passport';
import { userLocalStrategy } from './auth/strategy';
import { isLoggedInHTML, isLoggedInAPI } from './auth/guards';

import * as Knex from "knex";
import * as knexConfig from "./knexfile";

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const sessionMiddleware = expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave: true,
  saveUninitialized: true
});

app.use(sessionMiddleware);


// Passport is added as the middleware for our application
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function (user: { id: number }, done) {
  done(null, user);
});

passport.deserializeUser(function (user: { id: number }, done) {
  done(null, user);
});

passport.use('user-local', userLocalStrategy);


// public
app.use(express.static(path.join(__dirname, 'public')));
app.use('/img',express.static(path.join(__dirname, 'uploads')));

import { UserService } from './services/UserService';
import { UserRouter } from './routers/UserRouter';
export const userService = new UserService(knex);

//Users Image Multer
const userImageMulterStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads', 'users'));
  },
  filename: function (req, file, cb){
    cb(null, `${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
});

const userImageMulterLimits = {
  fileSize: 10000000
}

const userImageMulter = multer({storage:userImageMulterStorage, limits: userImageMulterLimits })


import { ShopService } from './services/ShopService';
import { ShopRouter } from './routers/ShopRouter';
//Shops Multer
const shopMulterStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads', 'shops'));
  },
  filename: function (req, file, cb) {
    cb(null, `${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
});

const shopMulterLimits = {
  fileSize: 10000000
}

const shopMulter = multer({ storage: shopMulterStorage, limits: shopMulterLimits })
const shopService = new ShopService(knex);

import { ServiceService } from './services/ServiceService';
import { ServiceRouter } from './routers/ServiceRouter';
//Service Multer
const serviceMulterStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, 'uploads', 'services'));
  },
  filename: function (req, file, cb) {
    cb(null, `${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
});

const serviceMulterLimits = {
  fileSize: 10000000
}

const serviceMulter = multer({ storage: serviceMulterStorage, limits: serviceMulterLimits })
const serviceService = new ServiceService(knex);

import { CategoryService } from './services/CategoryService';
import { CategoryRouter } from './routers/CategoryRouter';
const categoryService = new CategoryService(knex);

import { BookingService } from "./services/BookingService";
import { BookingRouter } from "./routers/BookingRouter";
const bookingService = new BookingService(knex)

app.use('/users', new UserRouter(userService, userImageMulter).router());
app.use('/shops', new ShopRouter(shopMulter, shopService, userService, serviceService, categoryService).router());
app.use('/service', new ServiceRouter(serviceMulter, shopService, serviceService).router());
app.use('/categories', new CategoryRouter(categoryService, shopService).router());

// private
app.use('/bookings', isLoggedInAPI, new BookingRouter(bookingService, shopService, userService, serviceService).router())
app.use(isLoggedInHTML, express.static(path.join(__dirname, 'private')));

// Start the server
const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});