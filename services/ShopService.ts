import * as Knex from "knex";
// import { hashPassword } from "../hash";

export interface OpeningHour {
    dayOfWeek: number;
    start: string;
    end: string;
}

export class ShopService {
    constructor(private knex: Knex) {

    }

    async register(username: string, password: string, email: string, companyName: string, description: string, phone: number, address: number, imageFileName: string, openingHours: OpeningHour[]) {
        const trx = await this.knex.transaction();

        try {
            const user = await trx('users').insert({ username, password, email }, ['id']);

            const shop = await trx('shops').insert({
                name: companyName,
                description,
                phone,
                address,
                image: imageFileName,
            }, ['id']);

            await trx('shops_staff').insert({
                user_id: user[0].id,
                shop_id: shop[0].id,
                role_id: 1,
            });


            const data = openingHours.map((openingHour) => {
                return {
                    shop_id: shop[0].id,
                    day_of_the_week_id: openingHour.dayOfWeek,
                    start_time: openingHour.start,
                    end_time: openingHour.end
                };
            });

            await trx('shop_opening_hours').insert(data);

            await trx.commit();
            return shop[0].id;
        } catch (err) {
            await trx.rollback();
            throw err;
        }
    }

    async findByUserID(userID: number) {
        try {
            const staff = await this.knex("shops_staff").where({ user_id: userID }).first();
            const shop = await this.knex("shops").where({ id: staff.shop_id }).first();
            return shop;
        } catch (err) {
            throw err;
        }
    }

    async get(id: number) {
        try {
            const shop = await this.knex("shops").where({ id: id }).first();

            return shop;

        } catch (err) {
            throw err;
        }
    }

    async getAllInfo(id: number) {
        try {
            const shop = await this.get(id);

            if (!shop) {
                return null;
            } else {
                const openingHours = await this.getOpeningHours(id);
                shop['openingHours'] = openingHours;

                return shop;
            }
        } catch (err) {
            throw err;
        }
    }

    async getOpeningHours(shopID: number) {
        const openingHours = await
            this.knex("shop_opening_hours")
                .select("name", "start_time", "end_time")
                .innerJoin("day_week_name", "day_week_name.id", "shop_opening_hours.day_of_the_week_id")
                .where({ shop_id: shopID })
                .orderBy("day_week_name.id");

        return openingHours;
    }
}

