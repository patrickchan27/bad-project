import * as Knex from "knex";
// import { hashPassword } from "../hash";

export class UserService {
    constructor(private knex: Knex) { }

    async findUserByUsername(username: string) {
        const user = await this.knex("users").where({ username }).first();
        return user;
    }

    async register(username: string, password: string, fullName: string, email: string, phoneNumber: number, image: string | null) {
        const trx = await this.knex.transaction();
        try {
            // insert into users
            await trx.raw(/*psql*/`INSERT INTO users (username, password, email) VALUES ('${username}', '${password}', '${email}')`);

            //Insert into profiles
            // Image => Null
            if (image != null) {
                // console.log("a")
                await trx.raw(/*psql*/`INSERT INTO profiles (phone, image, full_name, user_id) VALUES ('${phoneNumber}', '${image}', '${fullName}', (SELECT id from users WHERE username='${username}'))`)
            } else { //image => Not Null
                // console.log("b")
                await trx.raw(/*psql*/`INSERT INTO profiles (phone, full_name, user_id) VALUES ('${phoneNumber}', '${fullName}', (SELECT id from users WHERE username='${username}'))`)
            }

            await trx.commit();

        } catch (err) {
            await trx.rollback();
            throw err;
        }
    }

    async getUserInfo(id: number) {
        const user = await this.knex("users").select('id', 'username', 'email').where({ id }).first();
        
        if (!user) {
            return null;
        }

        return user;
    }

    async getUserProfile(id: number) {
        const profile = await this.knex("profiles").select('phone', 'image', 'full_name').where({ user_id: id }).first();
        return profile;
    }

    async checkUsernameExist(username: string) {
        const user = await this.knex("users").where({ username }).first();
        return user ? true : false;
    }

    async checkEmailExist(email: string) {
        const user = await this.knex("users").where({ email }).first();
        return user ? true : false;
    }

    async checkUserHaveShop(userID: number) {
        const haveShop = await this.knex("shops_staff").where('user_id', userID).first();
        return haveShop ? true : false;
    }
}