import * as Knex from "knex";

export class CategoryService {
    constructor(private knex: Knex) {

    }

    async getAll(shopID: number) {
        const categories = await this.knex("categories").select("id", "category").where({ shop_id: shopID });
        return categories;
    }
    
    async create(shopID: number, name: string) {
        const category = await this.knex('categories').insert({ 
            shop_id: shopID,
            category: name,
         }, ['id']);

         return category[0].id;
    }
}