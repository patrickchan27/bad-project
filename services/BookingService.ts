import * as Knex from "knex";

export interface Booking {
    shop_id: number,
    user_id: number,
    status_id: number,
    remark: string,
    date: string,
    start_time: string,
    end_time: string
}

export interface item {
    booking_id: number,
    service_id: number,
    service_setting_id: number,
    price: number
}

export class BookingService {
    constructor(private knex: Knex) {

    }

    async getAll(shopID: number) {
        const bookings = await this.knex('bookings')
            .select('bookings.id', 'bookings.user_id', 'items.service_id', 'items.service_setting_id', 'items.price', 'bookings.date', 'bookings.start_time', 'bookings.end_time', 'bookings.remark')
            .innerJoin('items', 'bookings.id', 'items.booking_id')
            .where({ shop_id: shopID })
            .orderBy(['bookings.date', 'bookings.start_time']);

        return bookings;
    }

    async getAllforUser(userID: number) {
        const userbookings = await this.knex('bookings')
            .select('bookings.id', 'bookings.shop_id', 'items.service_id', 'items.service_setting_id', 'items.price', 'bookings.date', 'bookings.start_time', 'bookings.end_time', 'bookings.remark')
            .innerJoin('items', 'bookings.id', 'items.booking_id')
            .where({ user_id: userID })
            .orderBy(['bookings.date', 'bookings.start_time']);

        return userbookings;
    }


    async createBooking(shopID: number, userID: number, statusID: number, remark: string, date: string, startTime: string, endTime: string) {
        const bookingObj: Booking = {
            shop_id: shopID,
            user_id: userID,
            status_id: statusID,
            remark: remark,
            date: date,
            start_time: startTime,
            end_time: endTime
        }
        const booking = await this.knex("bookings").insert(bookingObj, ['id']);
        return booking[0].id;
    }

    async createBookingItem(bookingID: number, serviceID: number, settingID: number, price: number) {
        const item: item = {
            booking_id: bookingID,
            service_id: serviceID,
            service_setting_id: settingID,
            price: price,
        }
        await this.knex("items").insert(item);
    }
}