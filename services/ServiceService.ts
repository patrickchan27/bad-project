import * as Knex from "knex";

interface SettingObject {
    working_day: number,
    duration: number,
    start_time: string,
    end_time: string,
    price: number,
    status: boolean,
    service_id: number
}

interface ServiceObject {
    title: string,
    description: string,
    image: string,
    display: boolean,
    shop_id: number,
    category_id: number
}

export class ServiceService {
    constructor(private knex: Knex) { }

    async addService(title: string, description: string, image: string, shopID: number, categoryID: number) {
        try {
            // console.log("title",title);
            // console.log("description",description);
            // console.log("image",image);
            // console.log("shopID",shopID);
            // console.log("categoryID",categoryID);

            const serviceObject: ServiceObject = {
                title: title,
                description: description,
                image: image,
                display: true, //added
                shop_id: shopID,
                category_id: categoryID
            }

            const service = await this.knex('services').insert(serviceObject, ['id']);
            return service[0].id;

        } catch (error) {
            console.error(error);
            throw Error
        }
    }

    async addSetting(serviceID: number, workingDay: number, duration: number, startTime: string, endTime: string, price: number, status: boolean) {
        try {
            const SettingObject: SettingObject = {
                working_day: workingDay,
                duration: duration,
                start_time: startTime,
                end_time: endTime,
                price: price,
                status: status,
                service_id: serviceID
            }

            console.log(SettingObject);
            

            await this.knex('service_setting').insert(SettingObject);
        } catch (error) {
            console.error(error);
            throw Error
        }
    }

    async listService(shopID: number) {
        const service = await this.knex("services").where({ shop_id: shopID });
        return service //array of multiple object
    }

    async listSetting(serviceID: number) {
        const serviceSetting = await this.knex("service_setting").where({ service_id: serviceID });
        return serviceSetting //array of multiple object
    }

    async getMinSettingPrice(serviceID: number) {
        const res = await this.knex("service_setting").min({ price: 'price' }).where({ service_id: serviceID }).first();
        return res;
    }

    async get(id: number) {
        const service = await this.knex("services").where({ id }).first();
        return service;
    }

    async getSetting(settingID: number) {
        const serviceSetting = await this.knex("service_setting").where({ id: settingID }).first();
        return serviceSetting;
    }

    //SELECT * FROM services INNER JOIN categories on category_id = categories.id ==> categories + services
    //SELECT * FROM services LEFT JOIN services_setting on id = services_setting.service_id ==> join services + (multiple setting)
}

