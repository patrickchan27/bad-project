import * as Knex from "knex";

export async function seed(knex: Knex): Promise<any> {
    // Deletes ALL existing entries
    await knex.raw(/*psql*/`TRUNCATE TABLE roles RESTART IDENTITY CASCADE`);
    await knex.raw(/*psql*/`TRUNCATE TABLE day_week_name RESTART IDENTITY CASCADE`);
    await knex.raw(/*psql*/`TRUNCATE TABLE status RESTART IDENTITY CASCADE`);

    // Inserts seed entries
    // 1. Roles Table => Owner, Staff
    await knex.raw(/*psql*/`INSERT INTO roles (name) VALUES (?),(?)`,
        ["Owner",
            "Staff"]);

    // 2. day_week_name => Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    await knex.raw(/*psql*/`INSERT INTO day_week_name (name) VALUES (?),(?),(?),(?),(?),(?),(?)`, [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"]);

    // 3. status => Waiting, Completed, Cancelled
    await knex.raw(/*psql*/`INSERT INTO status (status) VALUES (?),(?),(?)`, [
        "Waiting",
        "Completed",
        "Cancelled"]);
    
};
