
const checkboxes_day_week = document.querySelectorAll('input[name="checkbox_day_week"]');

for (const checkbox of checkboxes_day_week) {
    checkbox.addEventListener('click', function (event) {
        const timeStartDiv = document.querySelector(`#timeStart${this.value}`);
        const timeEndDiv = document.querySelector(`#timeEnd${this.value}`);
        if (checkbox.checked) {
            timeStartDiv.classList.remove("d-none");
            timeEndDiv.classList.remove("d-none");

            document.querySelector(`#dayOff${this.value}`).classList.add("d-none");
        } else {
            timeStartDiv.classList.add("d-none");
            timeEndDiv.classList.add("d-none");

            document.querySelector(`#dayOff${this.value}`).classList.remove("d-none");
        }
    })
}


for (let i = 0; i < checkboxes_day_week.length; i++) {
    const start_time = document.querySelector(`input[name="start_time${i + 1}"]`);
    const end_time = document.querySelector(`input[name="end_time${i + 1}"]`);

    let timeFromPicker = flatpickr(start_time, config = {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        onOpen: function () {
            start_time.classList.remove('is-invalid');
            let maxTime = end_time.value;
            if (maxTime !== "") {
                timeFromPicker.config.maxTime = maxTime;
            }
        }
    });

    let timeToPicker = flatpickr(end_time, config = {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        onOpen: function () {
            end_time.classList.remove('is-invalid');
            let minTime = start_time.value;
            if (minTime !== "") {
                timeToPicker.config.minTime = minTime;
            }
        }
    });
}

document.querySelector("#signup-form").addEventListener('submit', async function (e) {
    e.preventDefault();
    let formValidation = true;

    let formData = new FormData();
    let username = this.querySelector("input[name='username']").value;
    let password = this.querySelector("input[name='password']").value;
    
    formData.append('username', username);
    formData.append('email', this.querySelector("input[name='email']").value);
    formData.append('password', password);
    formData.append('companyName', this.querySelector("input[name='shop_name']").value);
    formData.append('phone', this.querySelector("input[name='phone']").value);
    formData.append('address', this.querySelector("input[name='address']").value);
    formData.append('description', this.querySelector("textarea[name='shop_desc']").value);
    formData.append('image', this.querySelector("input[name='image']").files[0])

    for (let i = 0; i < checkboxes_day_week.length; i++) {
        const checkbox = checkboxes_day_week[i];
        const start_time = document.querySelector(`input[name="start_time${i + 1}"]`);
        const end_time = document.querySelector(`input[name="end_time${i + 1}"]`);

        if (checkbox.checked) {
            if (start_time.value === "") {
                start_time.classList.add("is-invalid");
                formValidation = false;
            } else {
                start_time.classList.remove("is-invalid");
            }

            if (end_time.value === "") {
                end_time.classList.add("is-invalid");
                formValidation = false;
            } else {
                end_time.classList.remove("is-invalid");
            }

            formData.append('startTime', start_time.value);
            formData.append('endTime', end_time.value);
        } else {
            formData.append('startTime', '');
            formData.append('endTime', '');
        }
    }

    if (formValidation) {
        try {
            const res = await fetch('/shops/register', {
                method: "POST",
                body: formData,
            });

            const resJson = await res.json();

            if (res.status === 200) {
                await login(username, password);
            } else {
                alert(resJson.message);
            }
        } catch (error) {
            alert("Internal Error");
        }
    }

});

async function login(username, password) {
    const bodyObject = { username, password };
    const res = await fetch('/users/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(bodyObject)
    })
    if (res.status === 401) {
        const json = await res.json();
        window.alert("Bad Request!", json.message);
        throw Error
    } else {
        window.location.href = '/list_services.html'
    }
}

bsCustomFileInput.init();
