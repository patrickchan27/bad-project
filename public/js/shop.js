let id = 0;

async function getShopInfo(id) {
    const res = await fetch(`/shops/${id}`);
    const resJson = await res.json();

    if (res.status === 200) {
        return resJson.shop;
    } else {
        throw Error;
    }
}

function showShopInfo(shop) {
    console.log(shop);

    document.querySelector("#shopImage").src = `/img/shops/${shop.image}`;
    for (const titleHTML of document.querySelectorAll(".shop-title")) {
        titleHTML.innerHTML = shop.name;
    }
    document.querySelector(".shop-desc").innerHTML = shop.description;
    document.querySelector("#address").innerHTML = shop.address;
    document.querySelector("#phone").innerHTML = shop.phone;

    showOpeningHours(shop.openingHours);

    if (shop.notice !== null) {
        document.querySelector("#noticeRow").classList.remove("d-none");
        document.querySelector("#shopInfo").innerHTML = shop.notice;
    }
}

function showOpeningHours(openingHours) {
    const openingHoursNode = document.querySelector("#openingHours");
    for (const openingHour of openingHours) {
        const templateNode = document.querySelector(".openingHour-template").cloneNode(true);
        templateNode.classList.remove('openingHour-template')
        templateNode.querySelector(".openingHour-day").innerHTML = openingHour.name;
        templateNode.querySelector(".start-time").innerHTML = openingHour.start_time.slice(0, 5);
        templateNode.querySelector(".end-time").innerHTML = openingHour.end_time.slice(0, 5);
        openingHoursNode.appendChild(templateNode);
    }
}

function showServices(services) {
    const servicesNode = document.querySelector('#services');
    for (const service of services) {
        const templateNode = document.querySelector('.service-template').cloneNode(true);
        templateNode.classList.remove('service-template');
        templateNode.querySelector(".service-link").href = `/shop_service.html?id=${id}&serviceID=${service.id}`
        templateNode.querySelector(".service-img").src = `/img/services/${service.image}`;
        templateNode.querySelector(".service-title").innerHTML = service.title;
        templateNode.querySelector(".service-price").innerHTML = service.price;

        servicesNode.appendChild(templateNode);
    }

}

async function main() {
    const urlParams = new URLSearchParams(window.location.search);
    id = parseInt(urlParams.get('id'));

    try {
        if (id > 0 && typeof id == 'number') {
            const shop = await getShopInfo(id);
            showShopInfo(shop);
            showServices(shop.services)
        } else {
            throw Error;
        }
    } catch (error) {
        window.location.href = '/404.html';
    }


}

main();