AOS.init({
    duration: 1000
});

let action = '';
let booking;

//Login
document.getElementById("login-form").addEventListener('submit', async (event) => {
    event.preventDefault();

    try {
        const username = document.querySelector('#user_login').value;

        const password = document.querySelector('#password_login').value;

        const bodyObject = { username, password };
        const res = await fetch('/users/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(bodyObject)
        })
        if (res.status === 401) {
            const json = await res.json();
            window.alert(json.message);
            throw Error
        } else {
            const urlParams = new URLSearchParams(window.location.search);
            const action = urlParams.get('action');

            if (action === 'booking') {
                const bookingJson = localStorage.getItem('booking');
                const res = await fetch('/bookings/submit', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json; charset=utf-8",
                    },
                    body: bookingJson
                })
                const resJson = await res.json();
        
                if (res.status === 200) {
                    localStorage.removeItem('booking');
                    alert(resJson.message);
                    redirectLocation();
                } else if (res.status === 400) {
                    alert(resJson.message);
                } else {
                    alert("Network problem");
                }
            } else {
                redirectLocation();
            }
        }
    } catch (error) {
        console.log(error);
        alert("Error");
    }
})

document.getElementById("createAccount").addEventListener('click', function (event) {
    window.location.href = './userRegister.html'
});

async function redirectLocation() {
    const res = await fetch('/users/redirect');
    const jsonObj = await res.json();


    if (jsonObj.haveShop == true) {
        window.location.href = '/shop_booking.html'
    } else {
        window.location.href = '/user_booking.html'
    }

}