let shopID = 0;
let serviceID = 0;
let workingDaysPicker;

async function getServiceInfo(shopID, serviceID) {
    const res = await fetch(`/shops/${shopID}/${serviceID}`);
    const resJson = await res.json();

    if (res.status === 200) {
        return resJson;
    } else {
        throw error;
    }
}

function showShopServiceInfo(jsonObject) {
    console.log(jsonObject);
    const shop = jsonObject.shop;
    const service = jsonObject.service;
    const settings = jsonObject.settings;

    document.querySelector("#shopImage").src = `/img/services/${service.image}`;
    for (const titleHTML of document.querySelectorAll(".service-title")) {
        titleHTML.innerHTML = service.title;
    }
    document.querySelector(".description").innerHTML = service.description;
    // Date

    //[1,1,1,3,3,4,4,5,5]
    const tmpSortWorkingDay = settings.map(function (setting) {
        return setting.working_day
    })

    //1,2,3,4,5,6,7
    const sortWorkingDays = tmpSortWorkingDay.sort().reduce((init, current) => {
        if (init.length === 0 || init[init.length - 1] !== current) {
            init.push(current);
        }
        return init;
    }, []);

    console.log(sortWorkingDays);

    workingDaysPicker = flatpickr(document.querySelector('#date'), config = {
        dateFormat: "Y-m-d",
        minDate: new Date().fp_incr(1),
        maxDate: new Date().fp_incr(30),
        enable: [function (date) {
            for (const workingDay of sortWorkingDays) {
                if (moment(date).isoWeekday() === workingDay) {
                    return true;
                }
            }
            return false;
        }]
    });

    // const defaultstrinDay = (/*hrml*/`<option selected disabled>Please Select</option>`);
    // document.querySelector('#day').innerHTML = defaultstrinDay;

    // let optionArray = [];
    // for (let workingDay of sortWorkingDay) {
    //     const optionStr = (/*html*/`<option value="${workingDay}" 
    //         id="dayOption${workingDay}">
    //         ${workingDay}
    //         </option>`);
    //     optionArray.push(optionStr);
    // }

    // if (optionArray.length == 1) {
    //     document.querySelector('#day').innerHTML += optionArray[0];
    // } else {
    //     const options = optionArray.reduce((prev, curr) => prev + curr);

    //     document.querySelector('#day').innerHTML += options;
    // }
}

async function timeEvent(jsonObject) {
    document.querySelector('#date').addEventListener('change', (event) => {
        //default settings
        document.querySelector('#price').textContent = "";
        const defaultDisabledOption = '<option selected="selected" disabled>Please Select</option>';
        document.querySelector('#time').innerHTML = defaultDisabledOption;
        //Initial Value
        const settings = jsonObject.settings;
        const service = jsonObject.service;
        const isoWeekday = moment(event.target.value).isoWeekday();

        //filter

        function equaltoWorkingDay(obj) {
            return obj == isoWeekday;
        }
        function filterByday(item) {
            if (equaltoWorkingDay(item.working_day)) {
                return true;
            }
            return false;
        }

        let filterSettings = settings.filter(filterByday);

        let optionArray = [];
        for (let filtSetting of filterSettings) {
            const startTime = moment(filtSetting.start_time, 'H:mm:ss');
            const endTime = moment(filtSetting.end_time, 'H:mm:ss');
            const addedTime = moment(filtSetting.start_time, 'H:mm:ss').add(filtSetting.duration, 'h');

            while (addedTime.isSameOrBefore(endTime)) {
                const opt = document.createElement('option');
                opt.value = filtSetting.price;
                opt.dataset.settingID = filtSetting.id;
                opt.dataset.startTime = startTime.format('H:mm');
                opt.dataset.endTime = addedTime.format('H:mm');
                opt.text = `${startTime.format('H:mm')} - ${addedTime.format('H:mm')}`;
                document.querySelector('#time').add(opt);

                startTime.add(1, 'h');
                addedTime.add(1, 'h');
            }
        }
    })
}

async function priceEvent() {

    document.querySelector('#time').addEventListener('change', (event) => {
        const selectedOption = event.target.options[event.target.options.selectedIndex];
        const result = document.querySelector('#price');
        result.textContent = `$ ${event.target.value}`;
    });
}

document.querySelector('#booking-form').addEventListener('submit', async (event) => {
    event.preventDefault();

    // Input value from frontend
    const remark = document.querySelector('#remark').value;
    const date = document.querySelector('#date').value;

    const timeNode = document.querySelector('#time');
    const selectedOption = timeNode.options[timeNode.options.selectedIndex];
    const price = selectedOption.value;
    const settingID = selectedOption.dataset.settingID;
    const startTime = selectedOption.dataset.startTime;
    const endTime = selectedOption.dataset.endTime;

    const formObject = { 
        remark, 
        shopID: id, 
        serviceID, 
        settingID,
        price,
        date,
        startTime,
        endTime
    };

    console.log(formObject);
    

    try {
        const res = await fetch('/bookings/submit', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })
        const resJson = await res.json();

        if (res.status === 200) {
            localStorage.removeItem('booking');
            alert(resJson.message);
        } else if (res.status === 400) {
            alert(resJson.message);
        } else if (res.status === 401) {
            alert(resJson.message);
            localStorage.setItem('booking', JSON.stringify(formObject));
            window.location.href = "/userlogin.html?action=booking";
        } else {
            alert("Network problem");
        }
        
    } catch (error) {
        console.log(error);
        alert("Error");
    }
    
})

async function main() {
    const urlParams = new URLSearchParams(window.location.search);
    id = parseInt(urlParams.get('id'));
    serviceID = parseInt(urlParams.get('serviceID'));

    if (id > 0 && typeof id == 'number' && serviceID > 0 && typeof serviceID) {
        try {
            const jsonObject = await getServiceInfo(id, serviceID); //

            showShopServiceInfo(jsonObject);
            await timeEvent(jsonObject);
            await priceEvent();
        } catch (error) {
            console.log(error);
        }
    } else {
        window.location.href = '/404.html';
    }
}

main();