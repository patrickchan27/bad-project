document.getElementById("register-form").addEventListener("submit", async function (event) {
    event.preventDefault();
    const firstName = document.querySelector("#firstName_register").value;
    const lastName = document.querySelector("#lastName_register").value;
    const fullName = firstName + " " + lastName;

    const username = document.querySelector("#username_register").value
    const password = document.querySelector("#password_register").value

    if (password != document.querySelector("#confirm_password_register").value) {
        window.alert("unmatched password")
    } else {
        let formData = new FormData();
        formData.append('username', username);
        formData.append('password', password);
        formData.append('fullName', fullName);
        formData.append('email', document.querySelector("#email_register").value);
        formData.append('phoneNumber', document.querySelector("#phone_register").value);

        try {
            const res = await fetch('/users/register', {
                method: "POST",
                body: formData
            });

            const resJson = await res.json();

            if (res.status === 200) {
                await login(username, password);
            } else {
                alert(resJson.message)
            }
        } catch (error) {
            alert("Internal error");
        }
    }
})

async function login(username, password) {
    const bodyObject = { username, password };
    const res = await fetch('/users/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(bodyObject)
    })
    if (res.status === 401) {
        const json = await res.json();
        window.alert("Bad Request!", json.message);
        throw Error
    } else {
        redirectLocation();
    }
}

async function redirectLocation() {
    const res = await fetch('/users/redirect');
    const jsonObj = await res.json();


    if (jsonObj.haveShop == true) {
        window.location.href = '/shop_booking.html'
    } else {
        window.location.href = '/user_booking.html'
    }

}