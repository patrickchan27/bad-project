import { Request, Response, Router } from "express";
import { BookingService } from "../services/BookingService";
import { ShopService } from "../services/ShopService";
import { UserService } from "../services/UserService";
import { ServiceService } from "../services/ServiceService";

export class BookingRouter {
    constructor(private bookingService: BookingService, private shopService: ShopService, private userService: UserService, private serviceService: ServiceService) {

    }

    router() {
        const router = Router();
        router.get('/', this.getAllBookings);
        router.get('/user', this.getAllBookingsforUser)
        router.post('/submit', this.submitBooking);
        return router;
    }

    private getAllBookings = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const shop = await this.shopService.findByUserID(req.user["id"]);
                const shopID = shop.id;

                const bookings = await this.bookingService.getAll(shopID);
                if (bookings.length === 0) {
                    res.status(200).json({ result: [] });
                    return;
                }

                for (const booking of bookings) {
                    const userProfile = await this.userService.getUserProfile(booking.user_id);
                    const service = await this.serviceService.get(booking.service_id);
                    const setting = await this.serviceService.getSetting(booking.service_setting_id);
                    booking['user'] = userProfile;
                    booking['service'] = service;
                    booking['setting'] = setting;
                }

                res.status(200).json({ bookings });

            } else {
                throw Error;
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private getAllBookingsforUser = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const bookings = await this.bookingService.getAllforUser(req.user["id"]);
                if (bookings.length === 0) {
                    res.status(200).json({ result: [] });
                    return;
                }
                // get related id

                for (const booking of bookings) {
                    const userProfile = await this.shopService.get(booking.shop_id); // get shopService=>shop info
                    const service = await this.serviceService.get(booking.service_id);
                    const setting = await this.serviceService.getSetting(booking.service_setting_id);
                    booking['shop'] = userProfile;
                    booking['service'] = service;
                    booking['setting'] = setting;
                }

                res.status(200).json({ bookings });

            } else {
                throw Error;
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private submitBooking = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const userID = req.user["id"];
                const haveShop = await this.userService.checkUserHaveShop(userID);

                if (haveShop == false) {
                    const submitDetail = req.body;
                    const bookingId = await this.bookingService.createBooking(submitDetail.shopID, userID, 1, submitDetail.remark, submitDetail.date, submitDetail.startTime, submitDetail.endTime);
                    
                    
                    await this.bookingService.createBookingItem(bookingId, submitDetail.serviceID, submitDetail.settingID, submitDetail.price);

                    res.status(200).json({ message: "submitted" });
                } else {
                    res.status(400).json({
                        message: "Booking is not required for Shop Account"
                    })
                }
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }
}