import { Request, Response, Router } from "express";
import { ServiceService } from "../services/ServiceService"; //added
import { ShopService } from "../services/ShopService";
import * as multer from "multer";
import { isLoggedInAPI } from "../auth/guards";
// import { settings } from "cluster";

export class ServiceRouter {
    constructor(private upload: multer.Instance, private shopService: ShopService, private serviceService: ServiceService) {
    }

    router() {
        const router = Router();
        // router.post('/create', this.upload.single('image'), this.createService);
        router.get('/list', this.getServices);
        router.post('/create', isLoggedInAPI, this.upload.single('image'), this.createService);
        return router;
    }

    private createService = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const shop = await this.shopService.findByUserID(req.user["id"])
                const shopID = shop.id
                const title = req.body.title;
                const description = req.body.description;
                const image = req.file;
                const categoryID = req.body.category;

                console.log(image);
                

                let serviceID;
                if (image) {
                    serviceID = await this.serviceService.addService(title, description, image.filename, shopID, categoryID);
                } else {
                    serviceID = await this.serviceService.addService(title, description, "", shopID, categoryID);
                }

                const settings = JSON.parse(req.body.settings);
                for (let setting of settings) {
                    await this.serviceService.addSetting(serviceID, setting.workingDay, setting.duration, setting.startTime, setting.endTime, setting.price, setting.status)
                }

                res.status(200).json({ message: "created Service!" })
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "internal error" })
        }
    }

    private getServices = async (req: Request, res: Response) => {
        try {
            if (req.user && req.user["id"]) {
                const shop = await this.shopService.findByUserID(req.user["id"]);
                const shopID = shop.id;
                const services = await this.serviceService.listService(shopID);

                let settings = [];
                for (let service of services){
                   let setting = await this.serviceService.listSetting(service.id);
                    settings.push(setting)
                }
                res.status(200).json({ message: "lists all", services, settings});
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "internal error" })
        }

    }
}