import { Request, Response, Router } from "express";
import { CategoryService } from "../services/CategoryService";
import { ShopService } from "../services/ShopService";
import { isLoggedInAPI } from "../auth/guards";

export class CategoryRouter {
    constructor(private categoryService: CategoryService, private shopService: ShopService) {
        
    }

    router() {
        const router = Router();
        router.get('/', isLoggedInAPI, this.getAll)
        router.post('/', isLoggedInAPI, this.post);
        return router;
    }

    private getAll = async (req: Request, res: Response) => {
        try {
            if(req.user) {
                const shop = await this.shopService.findByUserID(req.user['id'])

                if (shop) {
                    const categories = await this.categoryService.getAll(shop.id);
                    res.status(200).json({ categories });
                } else {
                    res.status(400).json({ message: "Not allow" });
                }
            } else {
                throw Error;
            }

            
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private post = async (req: Request, res: Response) => {
        try {
            const { name } = req.body;
            if(req.user) {
                const shop = await this.shopService.findByUserID(req.user['id'])

                if (shop) {
                    const categoryID = await this.categoryService.create(shop.id, name);

                    res.status(200).json({ message: "New shop created", categoryID });
                } else {
                    res.status(400).json({ message: "Not allow" });
                }
            } else {
                throw Error;
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }
}