import { Request, Response, Router } from "express";
import { ShopService, OpeningHour } from "../services/ShopService";
import { UserService } from "../services/UserService";
import { ServiceService } from "../services/ServiceService";
import { CategoryService } from "../services/CategoryService";
import * as multer from "multer";
import { hashPassword } from "../auth/hash";

export class ShopRouter {
    constructor(private upload: multer.Instance, private shopService: ShopService, private userService: UserService, private serviceService: ServiceService, private categoryService: CategoryService) {

    }

    router() {
        const router = Router();
        router.get('/:id', this.get);
        router.post('/logout', this.logout);
        router.post('/register', this.upload.single('image'), this.register);
        router.get('/:id/:serviceID', this.getShopServiceDetail);
        router.get('/:id/categories', this.getCategories);
        return router;
    }

    private logout = (req: Request, res: Response) => {
        req.logOut();
        res.redirect("/");
    };

    private register = async (req: Request, res: Response) => {
        try {

            const { username, password, email, companyName, description, phone, address, startTime, endTime } = req.body
            const image = req.file;
            const openingHours: OpeningHour[] = [];

            const isUsernameExist = await this.userService.checkUsernameExist(username);
            if (isUsernameExist) {
                res.status(409).json({ message: "Username already exists" });
                return;
            }

            const isEmailExist = await this.userService.checkEmailExist(email);
            if (isEmailExist) {
                res.status(409).json({ message: "Email already exists" });
                return;
            }

            for (let i = 0; i < startTime.length; i++) {
                if (startTime[i] !== "") {
                    openingHours.push({
                        dayOfWeek: i + 1,
                        start: startTime[i],
                        end: endTime[i],
                    })
                }
            }

            let shopID;
            const hashpassword = await hashPassword(password);

            if (req.file) {
                shopID = await this.shopService.register(username, hashpassword, email, companyName, description, phone, address, image.filename, openingHours);
            } else {
                shopID = await this.shopService.register(username, hashpassword, email, companyName, description, phone, address, "", openingHours);
            }

            res.status(200).json({ message: "New shop created", shopID });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private get = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            if (isNaN(id)) {
                res.status(404).json({ message: "Need a valid shop id" });
                return;
            }

            const shop = await this.shopService.getAllInfo(id);
            if (shop === null) {
                res.status(404).json({ message: "Internal Error" });
                return;
            }

            const services = await this.serviceService.listService(id);
            if (services) {
                for (const service of services) {
                    const res = await this.serviceService.getMinSettingPrice(service.id);
                    if (res) {
                        service['price'] = res['price'];
                    }
                }
            }

            shop['services'] = services;

            res.status(200).json({ shop });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private getShopServiceDetail = async (req: Request, res: Response) => {
        try {
            // console.log("getShopServiceDetail", req.params);

            const id = parseInt(req.params.id);
            if (isNaN(id)) {
                throw Error;
            }
            const serviceID = parseInt(req.params.serviceID);
            if (isNaN(serviceID)) {
                throw Error;
            }

            // console.log(id, serviceID)


            const shop = await this.shopService.getAllInfo(id);
            if (shop === null) {
                res.status(404).json({ message: "Internal Error" });
                return;
            }

            const services = await this.serviceService.listService(id)
            if (services === null) {
                res.status(404).json({ message: "Internal Error" });
                return;
            }

            const service = services.find(element => element.id == serviceID)//array
            console.log(services);
            console.log(service);
            
            const settings = await this.serviceService.listSetting(service.id)
            if (settings === null) {
                res.status(404).json({ message: "No settings" });
            }

            res.status(200).json({ message: "listed", shop, service, settings });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }

    private getCategories = async (req: Request, res: Response) => {
        try {
            const id = parseInt(req.params.id);
            if (isNaN(id)) {
                res.status(404).json({ message: "Need a valid shop id" });
                return;
            }

            const categories = await this.categoryService.getAll(id);
            res.status(200).json({ categories });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Internal Error" })
        }
    }
}
