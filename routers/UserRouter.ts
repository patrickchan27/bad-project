import { Request, Response, NextFunction, Router } from "express";
import { UserService } from "../services/UserService";
import * as passport from 'passport';
import {hashPassword} from "../auth/hash";
import * as multer from "multer";

export class UserRouter {
    constructor(private userService: UserService, private upload: multer.Instance) { } //multer

    router() {
        const router = Router();

        router.post('/login', (...rest) => passport.authenticate('user-local', this.login(...rest))(...rest));
        router.post('/logout', this.logout);
        router.post('/register', this.upload.single('image'), this.register);
        router.get('/redirect', this.redirect)
        return router;
    }

    private login = (req: Request, res: Response, next: NextFunction) => {
        return (err: Error,
            user: { id: number, username: string },
            info: { message: string }) => {

            if (err) {
                res.status(401).json({ message: err.message });
            } else if (info && info.message) {
                res.status(401).json({ message: info.message });
            } else {
                req.logIn(user, (err) => {
                    if (err) {
                        res.status(401).json({ message: "Failed to Login" });
                    } else {
                        res.status(200).json({ message: "Login successful" });
                    }
                });
            }
        };
    }

    private logout = (req: Request, res: Response) => {
        console.log("logout");

        req.logOut();
        res.redirect("/");
    };

    //added
    private register = async (req: Request, res: Response) => {
        try {
            const username = req.body.username;

            const password = req.body.password;
            const hashpassword = await hashPassword(password);

            // const firstName:string = req.body.firstName;
            // const lastName:string = req.body.lastName;
            
            const fullName = req.body.fullName;

            const email = req.body.email;

            const phoneNumber = parseInt(req.body.phoneNumber);

            const image = req.body.image;

            const isUsernameExist = await this.userService.checkUsernameExist(username);
            if (isUsernameExist) {
                res.status(409).json({ message: "Username already exists" });
                return ;
            }

            const isEmailExist = await this.userService.checkEmailExist(email);
            if (isEmailExist) {
                res.status(409).json({ message: "Email already exists" });
                return ;
            }

            await this.userService.register(username, hashpassword, fullName, email, phoneNumber, image);
            res.status(200).json({ result: true, message: 'Register successfully' });
        } catch (err) {
            res.status(400).json({ message: err.message });
            throw Error
        }
    };

    private redirect = async (req:Request, res:Response) => {
        try {
            if (req.user && req.user["id"]) {
                
                const userID = req.user["id"];
                const haveShop = await this.userService.checkUserHaveShop(userID);

                res.status(200).json({message:"redirect", haveShop})
            }
        } catch (error) {
             res.status(500).json({ message: "internal Error" });
        }
    }
}