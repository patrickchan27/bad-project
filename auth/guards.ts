import { Request, Response, NextFunction } from 'express';

export function isLoggedInHTML(req: Request, res: Response, next: NextFunction) {
    if (req.user) {
        next();
    } else {
        res.redirect('/userlogin.html');
    }
}

export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.user) {
        next();
    } else {
        res.status(401).json({
            message: "Requires authentication"
        });
    }
}
