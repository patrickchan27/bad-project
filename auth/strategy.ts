import * as passportLocal from 'passport-local';
import { checkPassword } from "./hash";
import { userService } from "../app";

const LocalStrategy = passportLocal.Strategy;

export const userLocalStrategy = new LocalStrategy(
    async function (username, password, done) {
       try {
           const user = await userService.findUserByUsername(username);
        if (!user) {
            return done(null, false, { message: 'Incorrect username or password!' });
        }
        const match = await checkPassword(password, user.password);
        if (match) {
            return done(null, { id: user.id });
        } else {
            return done(null, false, { message: 'Incorrect username or password!' });
        }
       } catch (error) {
           console.log(error.message);
           return done(null, false, { message: "internal error" });
       }
        
    }
)

