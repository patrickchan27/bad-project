import * as Knex from "knex";
const knexfile = require('../knexfile'); // Assuming you test case is inside `services/ folder`
const knex = Knex(knexfile["testing"]); // Now the connection is a testing connection.
import { UserService } from '../services/UserService';
import { hashPassword } from "../auth/hash";

describe("UserService", () => {
    let userService: UserService;

    beforeEach(async () => {
        userService = new UserService(knex);
        await knex.raw(/*sql*/`DELETE from profiles`);
        await knex.raw(/*sql*/`DELETE from users`);

        const trx = await knex.transaction();

        try {
            const hashedpassword = await hashPassword('test');
            const result = await trx.raw(/*sql*/`INSERT INTO users (username, password, email) VALUES (?, ?, ?) RETURNING id`, ['user', hashedpassword, 'user@test.com']);
            const newUserID = result.rows[0].id;

            await trx.raw(/*sql*/`INSERT INTO profiles (phone, image, full_name, user_id) VALUES (?, ?, ?, ?)`, [24242424, 'test.jpg', 'Jason Li', newUserID]);

            await trx.commit();
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    })

    it("Check username exist" , async () => {
        const result = await userService.checkUsernameExist("user");
        expect(result).toBe(true);
    });

    afterAll(() => {
        knex.destroy();
    })
})